//
//  RTCApp.swift
//  RTC
//
//  Created by Alberto Dos Santos Ribeiro on 19/04/21.
//

import SwiftUI

@main
struct RTCApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
